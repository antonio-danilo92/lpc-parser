package com.ssr.privacyguard.utility;

import android.util.Log;

import com.ssr.privacyguard.obj.Manifest;
import com.ssr.privacyguard.obj.Permission;

/**
 * RiskCalculator using the formula: (cat1norm+cat2norm)*cat3max
 * NOT TESTED
 */
public class CategoryRiskCalculator implements RiskCalculator{
   // private Manifest manifest;
    private int categoryMax[]={71,351,119};
    private int categoryWeight[]={3,7,1};
    public CategoryRiskCalculator(){
        //this.manifest= manifest;
        /* si può aggiungere il calcolo automatico della somma dei punteggi per categoria.
         In questo modo sarebbe possibile calcolare la normalizzazione in modo dimanico
          */
    }

    //TODO menage permission category SPECIAL (bind_device_admin, acces_superuser, bind_accesibility_service)
    @Override
    public double calcRisk(Manifest manifest) {
        int catScore[]={0,0,0};
        int tmpScore;
        for(Permission p:manifest.getPermissionList()){
            //if (PermissionChecker.checkPermission(p,manifest.getPackageName())) {
                tmpScore = p.getPunteggio();
                switch (p.getCategory()) {
                    case Permission.HARDWARE_ACCESS:
                        catScore[0] += tmpScore;
                        break;
                    case Permission.DATA_ACCESS:
                        catScore[1] += tmpScore;
                        break;
                    case Permission.COMMUNICATION:
                        catScore[2] = (catScore[2] <= tmpScore) ? tmpScore : catScore[2];
                }
            }
        //}
        double somma=0;
        double[] normCatScore=new double[3];
        for(int i=0;i<categoryMax.length-1;i++){
            normCatScore[i]=((double) catScore[i]/categoryMax[i])*categoryWeight[i];
            somma=somma+normCatScore[i];
        }
        return (normCatScore[0]+normCatScore[1])*catScore[2];
    }
}
