package com.ssr.privacyguard.utility;

import com.ssr.privacyguard.obj.Manifest;

/**
 * Created by Antonio on 06/12/2016.
 */

public interface RiskCalculator {
    double calcRisk(Manifest manifest);
}
