package com.ssr.privacyguard.utility;

import android.Manifest;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.service.notification.NotificationListenerService;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.util.Log;
import android.view.accessibility.AccessibilityManager;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;

import com.ssr.privacyguard.MainApplication;
import com.ssr.privacyguard.obj.Permission;

import java.util.List;


/**
 * Created by Antonio on 07/12/2016.
 */

public class PermissionChecker extends AppCompatActivity {


    public static boolean checkPermission(Permission p, String appName){
        switch(p.getPermissionName()){
            case "android.permission.CHANGE_CONFIGURATION" :
                return changeConfigurationCheck();
            case "android.permission.BIND_INPUT_METHOD":
                return bindInputMethodCheck(appName);
            case "android.permission.PACKAGE_USAGE_STATS":
                return packageUsageStatsCheck(appName);
            case "android.permission.BIND_NOTIFICATION_LISTENER_SERVICE":
                return bindNotificationListenerServiceCheck(appName);
            case "android.permission.BIND_ACCESSIBILITY_SERVICE":
                return bindAccessibilityServiceCheck(appName);
            case "android.permission.BIND_DEVICE_ADMIN":
                return bindDeviceAdminCheck(appName);
            case "android.permission.WRITE_SETTINGS":
                return bindWriteSettingsCheck(appName);
            case "android.permission.BIND_TELECOM_CONNECTION_SERVICE":
                return bindTelecomConnectionServiceCheck(appName);
            default:
                return permissionCheck(appName,p.getPermissionName());
        }
    }

    private static boolean changeConfigurationCheck(){
        int currentVersion= Build.VERSION.SDK_INT;
        return currentVersion <= Build.VERSION_CODES.JELLY_BEAN;
    }

    private static boolean bindInputMethodCheck(String appName){
        Context c= MainApplication.getAppContext();
        InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        List<InputMethodInfo> mInputMethodProperties = imm.getEnabledInputMethodList();
        final int N = mInputMethodProperties.size();
        if (mInputMethodProperties!=null)
            for (InputMethodInfo imi:mInputMethodProperties)
                if (imi.getPackageName().equals(appName))
                    return true;
        return false;
    }

    private static boolean packageUsageStatsCheck(String appName){
        Context c= MainApplication.getAppContext();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            UsageStatsManager usm= (UsageStatsManager)c.getSystemService(Context.USAGE_STATS_SERVICE);
            return !usm.isAppInactive(appName);
        }
        return false;
    }

    private static boolean bindWriteSettingsCheck (String appName){
        int currentVersion= Build.VERSION.SDK_INT;
        return currentVersion <= 23;
    }
    private static boolean bindNotificationListenerServiceCheck(String appName){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            ServiceListing sl=new ServiceListing(MainApplication.getAppContext(),getNotificationListenerConfig());
            sl.reload();
            return sl.isEnabled(appName);
        }
        else return true;
    }

    private static boolean bindAccessibilityServiceCheck(String appName){
        Context c= MainApplication.getAppContext();
        AccessibilityManager manager = (AccessibilityManager) c.getSystemService(Context.ACCESSIBILITY_SERVICE);
        List<AccessibilityServiceInfo> la=manager.getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_ALL_MASK);
        boolean eq=false;
        for (int i=0; i<la.size();i++) {
            AccessibilityServiceInfo asi = la.get(i);
            String packageName=ComponentName.unflattenFromString(asi.getId()).getPackageName();
            eq=packageName.equalsIgnoreCase(appName);
            if (eq)
                break;
        }
        return eq;
    }
    private static boolean bindDeviceAdminCheck(String appName){
        Context c= MainApplication.getAppContext();
        DevicePolicyManager manager = (DevicePolicyManager) c.getSystemService(DEVICE_POLICY_SERVICE);
        List<ComponentName> lc=manager.getActiveAdmins();
        if (lc!=null)
            for (ComponentName cn:lc)
                if (cn.getPackageName().equals(appName))
                    return true;
        return false;
    }

    private static boolean bindTelecomConnectionServiceCheck(String appName){
        Context c= MainApplication.getAppContext();
        ComponentName cn;
        if (Build.VERSION.SDK_INT>=23) {
            int permissionCheck = ContextCompat.checkSelfPermission(c.getApplicationContext(),
                    Manifest.permission.READ_PHONE_STATE);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                TelecomManager telecomManager = (TelecomManager) c.getSystemService(Context.TELECOM_SERVICE);
                List<PhoneAccountHandle> phoneAccountHandles = telecomManager.getCallCapablePhoneAccounts();
                if (phoneAccountHandles != null)
                    for (PhoneAccountHandle p : phoneAccountHandles) {
                        cn = p.getComponentName();
                        if (cn.getPackageName().equalsIgnoreCase(appName))
                            return true;
                    }
            }
        }
        return false;
    }

    private static boolean permissionCheck(String appName, String permissionName) {

        Context c=MainApplication.getAppContext();
        PackageManager packageManager=c.getPackageManager();
        int isActive=packageManager.checkPermission(permissionName,appName);
        //Log.d("permission",appName+" "+permissionName);
        return isActive == PackageManager.PERMISSION_GRANTED;
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    private static ServiceListing getNotificationListenerConfig() {
        final ServiceListing.Config c = new ServiceListing.Config();
        c.intentAction = NotificationListenerService.SERVICE_INTERFACE;
        c.permission = android.Manifest.permission.BIND_NOTIFICATION_LISTENER_SERVICE;
        c.noun = "notification listener";
        c.setting = "enabled_notification_listeners";

        return c;
    }
}