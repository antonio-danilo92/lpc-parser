package com.ssr.privacyguard.utility;

/**
 * Created by Antonio on 29/12/2016.
 */

public class MathUtility {

    //penalty for combination of subcatogory
    /*
    public static double ACF=1.02;
    public static double ACG=1.05;
    public static double ACH=1.09;
    public static double AEF=1.02;
    public static double AEG=1.06;
    public static double AEH=1.09;
    public static double BCF=1.05;
    public static double BCG=1.07;
    public static double BCH=1.10;
    public static double BEF=1.06;
    public static double BEG=1.08;
    public static double BEH=1.10;*/

    public static double ACF=1.01;
    public static double ACG=1.025;
    public static double ACH=1.045;
    public static double AEF=1.01;
    public static double AEG=1.03;
    public static double AEH=1.045;
    public static double BCF=1.025;
    public static double BCG=1.035;
    public static double BCH=1.05;
    public static double BEF=1.03;
    public static double BEG=1.04;
    public static double BEH=1.05;

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}
