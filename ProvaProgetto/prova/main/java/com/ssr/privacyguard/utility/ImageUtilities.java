package com.ssr.privacyguard.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.content.res.AppCompatResources;

import com.ssr.privacyguard.MainApplication;
import com.ssr.privacyguard.R;

/**
 * Created by Antonio on 29/12/2016.
 */

public class ImageUtilities {
    public static Drawable idToDrawable (int id){
        Context c=MainApplication.getAppContext();
        switch (id){
            case 1: return AppCompatResources.getDrawable(c,R.drawable.ic_action_system);
            case 2: return AppCompatResources.getDrawable(c,R.drawable.ic_action_location);
            case 3: return AppCompatResources.getDrawable(c,R.drawable.ic_action_notification);
            case 4: return AppCompatResources.getDrawable(c,R.drawable.ic_action_network);
            case 5: return AppCompatResources.getDrawable(c,R.drawable.ic_supersu);
            case 6: return AppCompatResources.getDrawable(c,R.drawable.ic_action_wifi);
            case 7: return AppCompatResources.getDrawable(c,R.drawable.ic_action_admin);
            case 8: return AppCompatResources.getDrawable(c,R.drawable.ic_action_sms);
            case 9: return AppCompatResources.getDrawable(c,R.drawable.ic_action_os_function);
            case 10: return AppCompatResources.getDrawable(c,R.drawable.ic_action_phone);
            case 11: return AppCompatResources.getDrawable(c,R.drawable.ic_action_keyboard);
            case 12: return AppCompatResources.getDrawable(c,R.drawable.ic_action_music);
            case 13: return AppCompatResources.getDrawable(c,R.drawable.ic_action_nfc);
            case 14: return AppCompatResources.getDrawable(c,R.drawable.ic_action_print);
            case 15: return AppCompatResources.getDrawable(c,R.drawable.ic_action_tv);
            case 16: return AppCompatResources.getDrawable(c,R.drawable.ic_action_voice);
            case 17: return AppCompatResources.getDrawable(c,R.drawable.ic_action_image);
            case 18: return AppCompatResources.getDrawable(c,R.drawable.ic_action_bluetooth);
            case 19: return AppCompatResources.getDrawable(c,R.drawable.ic_action_camera);
            case 20: return AppCompatResources.getDrawable(c,R.drawable.ic_action_contacts);
            case 21: return AppCompatResources.getDrawable(c,R.drawable.ic_action_search);
            case 22: return AppCompatResources.getDrawable(c,R.drawable.ic_action_calendar);
            case 23: return AppCompatResources.getDrawable(c,R.drawable.ic_action_storage);
            case 24: return AppCompatResources.getDrawable(c,R.drawable.ic_action_sync);
            case 25: return AppCompatResources.getDrawable(c,R.drawable.ic_action_battery);
            case 26: return AppCompatResources.getDrawable(c,R.drawable.ic_action_time);
            case 27: return AppCompatResources.getDrawable(c,R.drawable.ic_action_fingerprint);
            case 28: return AppCompatResources.getDrawable(c,R.drawable.ic_action_ir);
            case 29: return AppCompatResources.getDrawable(c,R.drawable.ic_action_vibration);
            case 30: return AppCompatResources.getDrawable(c,R.drawable.ic_action_alarm);
            case 31: return AppCompatResources.getDrawable(c,R.drawable.ic_action_voicemail);





            default: return null;
        }
    }
    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
}
