package com.ssr.privacyguard.utility;

/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

        import android.content.BroadcastReceiver;
        import android.content.ComponentName;
        import android.content.ContentResolver;
        import android.content.Context;
        import android.content.Intent;
        import android.content.pm.PackageManager;
        import android.content.pm.ResolveInfo;
        import android.content.pm.ServiceInfo;
        import android.provider.Settings;
        import android.util.Log;

        import java.util.ArrayList;
        import java.util.HashSet;
        import java.util.List;

public class ServiceListing {
    private final ContentResolver mContentResolver;
    private final Context mContext;
    private final Config mConfig;
    private final HashSet<ComponentName> mEnabledServices = new HashSet<ComponentName>();
    private final List<ServiceInfo> mServices = new ArrayList<ServiceInfo>();

    public ServiceListing(Context context, Config config) {
        mContext = context;
        mConfig = config;
        mContentResolver = context.getContentResolver();
    }





    protected static int getServices(Config c, List<ServiceInfo> list, PackageManager pm) {
        int services = 0;
        if (list != null) {
            list.clear();
        }
        //hiddem method. on the IDE gives error, but it exists
        @SuppressWarnings("unchecked")
        List<ResolveInfo> installedServices = pm.queryIntentServices(
                new Intent(c.intentAction),PackageManager.GET_META_DATA);

        for (int i = 0, count = installedServices.size(); i < count; i++) {
            ResolveInfo resolveInfo = installedServices.get(i);
            ServiceInfo info = resolveInfo.serviceInfo;

            if (!c.permission.equals(info.permission)) {
                Log.w(c.tag, "Skipping " + c.noun + " service "
                        + info.packageName + "/" + info.name
                        + ": it does not require the permission "
                        + c.permission);
                continue;
            }
            if (list != null) {
                list.add(info);
            }
            services++;
        }
        return services;
    }

    private void loadEnabledServices() {
        mEnabledServices.clear();
        final String flat = Settings.Secure.getString(mContentResolver, mConfig.setting);
        if (flat != null && !"".equals(flat)) {
            final String[] names = flat.split(":");
            for (int i = 0; i < names.length; i++) {
                final ComponentName cn = ComponentName.unflattenFromString(names[i]);
                if (cn != null) {
                    mEnabledServices.add(cn);
                }
            }
        }
    }

    public List<ServiceInfo> reload() {
        loadEnabledServices();
        getServices(mConfig, mServices, mContext.getPackageManager());

        return mServices;
    }

    public boolean isEnabled(String appName) {
        boolean eq=false;
        for (ComponentName cn:mEnabledServices) {
            eq = cn.getPackageName().equalsIgnoreCase(appName);
            if (eq) break;
        }
        return eq;
    }



    public static class Config {
        public String tag;
        public String setting;
        public String secondarySetting;
        public String intentAction;
        public String permission;
        public String noun;
        public int warningDialogTitle;
        public int warningDialogSummary;
        public int emptyText;
    }
}
