package com.ssr.privacyguard;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.util.Log;

import com.ssr.privacyguard.db.DBManager;

import java.util.Locale;

/**
 * Created by Antonio on 20/12/2016.
 */

public class MainApplication extends Application {
    private final String TAG="MainApplication";
    private static MainApplication mainApplication;
    private String currLanguage;
    public MainApplication(){
        mainApplication=this;
        currLanguage=Locale.getDefault().getLanguage();
        Log.d(TAG,"MainApplication statically assigned");
    }



    @Override
    public void onCreate() {
        super.onCreate();
        DBManager.initDB();

    }
    public static Context getAppContext(){
        return mainApplication;
    }
}
