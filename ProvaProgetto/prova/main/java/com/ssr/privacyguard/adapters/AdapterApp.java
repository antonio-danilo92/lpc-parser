package com.ssr.privacyguard.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ssr.privacyguard.R;
import com.ssr.privacyguard.fragments.FragmentList;
import com.ssr.privacyguard.obj.Application;

import java.text.NumberFormat;
import java.util.ArrayList;

import az.plainpie.PieView;

/**
 * Adapter handling the list of apps showed in the first screen.
 */
public class AdapterApp extends RecyclerView.Adapter<AdapterApp.AppViewHolder>{
    private final FragmentList.OnListFragmentInteractionListener listener; //used as a callback on pressing an app
    private Context context;
    private ArrayList<Application> allApps; //list al all apps that needs to be showed


    public AdapterApp(ArrayList<Application> list, FragmentList.OnListFragmentInteractionListener listener){
        allApps=list;
        this.listener=listener;
    }
    @Override
    public AppViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context=parent.getContext();
        View v = LayoutInflater.from(context).inflate(R.layout.row_single_app, parent, false);
        return new AppViewHolder(v);
    }
    @Override
    public long getItemId(int position) {
        if (hasStableIds())
            return allApps.get(position).hashCode();  //garanteed stable IDs, useful to animate the list
        else
            return RecyclerView.NO_ID; //return NO_ID if the list has no stable ids. has defined by getIdemId documentation
    }
    @Override
    public void onBindViewHolder(final AppViewHolder holder, int position) {
        holder.name.setText(allApps.get(position).getAppName()); // set app name
        holder.mApp=allApps.get(position); //save the current analyzed app in the holder
        holder.itemView.setOnClickListener(new View.OnClickListener() { //Onclick on the item
            @Override
            public void onClick(View v) {
                listener.onListFragmentInteraction(holder.mApp);
            }
        });
        //set the lenght of the bar around the score text
        double scoreF= allApps.get(position).getPrivacyScore();
        if(scoreF!=0)
            holder.pieScore.setPercentage((float) scoreF);
        else
            holder.pieScore.setPercentage(1); //workaround, you can't set 0 as percentage


        NumberFormat f = NumberFormat.getInstance();
        f.setMaximumFractionDigits(1); //truncate the score to 1 decimal numebr
        holder.pieScore.setInnerText(f.format(scoreF)+"%"); // set the score text as a percentage

        //set color of the bar around the score text
        if(scoreF>= context.getResources().getInteger(R.integer.threshold_red))
            holder.pieScore.setPercentageBackgroundColor(ContextCompat.getColor(context,R.color.md_red_500));
        else if(scoreF>= context.getResources().getInteger(R.integer.threshold_yellow))
            holder.pieScore.setPercentageBackgroundColor(ContextCompat.getColor(context,R.color.md_yellow_800));
        else
            holder.pieScore.setPercentageBackgroundColor(ContextCompat.getColor(context,R.color.md_green_500));

        //set app icon for the item considered
        Bitmap icon= allApps.get(position).getAppIcon();
        holder.iconImageView.setImageBitmap(icon);
    }

    @Override
    public int getItemCount() {
        return allApps.size();
    }

    /**
     * Holder for an app item
     */
    class AppViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private PieView pieScore;
        private ImageView iconImageView;
        private Application mApp;
        AppViewHolder(View itemView) {
            super(itemView);
            name= (TextView) itemView.findViewById(R.id.textview_app_name);
            pieScore=(PieView) itemView.findViewById(R.id.pieView_row);
            iconImageView =(ImageView) itemView.findViewById(R.id.imageview_logo_app);
            itemView.setClickable(false);
        }

    }
}
