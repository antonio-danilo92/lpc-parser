package com.ssr.privacyguard.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ssr.privacyguard.R;
import com.ssr.privacyguard.obj.Permission;
import com.ssr.privacyguard.utility.ImageUtilities;

import java.util.Collections;
import java.util.List;

import static android.view.View.GONE;


public class PermissionRecyclerViewAdapter extends RecyclerView.Adapter<PermissionRecyclerViewAdapter.ViewHolder> {

    private final List<Permission> mPermissions;
    private Context c;
    public PermissionRecyclerViewAdapter(List<Permission> items) {
        mPermissions = items;
        Collections.sort(mPermissions);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        c=parent.getContext();
        View view = LayoutInflater.from(c)
                .inflate(R.layout.fragment_permission, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mPermissions.get(position);
        //set description
        holder.mDescriptionView.setText(mPermissions.get(position).getDescription());
        //set name
        String permissionName=holder.mItem.getPermissionName();
        String strippedPermissionName=permissionName.substring(permissionName.lastIndexOf(".")+1);
        holder.mNameView.setText(strippedPermissionName);
        //set icon
        Drawable permicon=ImageUtilities.idToDrawable(holder.mItem.getImageId());
        if(permicon==null)
            permicon= AppCompatResources.getDrawable(c,R.mipmap.ic_app_default);
        holder.mPermImageView.setImageDrawable(permicon);

        //check for warning
        holder.mTextViewWarning.setVisibility(GONE);
        if(holder.mItem.getWarning()==null)
            holder.mWarningImage.setVisibility(GONE);
        else {
            holder.mWarningImage.setVisibility(View.VISIBLE);
            //check if warning is alert
            if(holder.mItem.getSubCategory()==Permission.SPECIAL) {
                holder.mWarningImage.setImageResource(R.mipmap.ic_error_red);
                holder.mTextViewWarning.setBackgroundResource(R.drawable.border_red);
            }
            //set onClickListener to show/hide the warning message
            holder.mLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(holder.mTextViewWarning.getVisibility()== GONE) {
                        holder.mTextViewWarning.setVisibility(View.VISIBLE);
                        holder.mTextViewWarning.setText(holder.mItem.getWarning());
                    }
                    else
                        holder.mTextViewWarning.setVisibility(GONE);
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return mPermissions.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mNameView;       //permission name
        final TextView mDescriptionView;        //permission description
        final ImageView mPermImageView;         //permission icon
        ImageView mWarningImage;                //warning image
        TextView mTextViewWarning;              //warning test
        RelativeLayout mLayout;
        Permission mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mPermImageView=(ImageView) view.findViewById(R.id.permission_image);
            mNameView = (TextView) view.findViewById(R.id.permission_name);
            mDescriptionView = (TextView) view.findViewById(R.id.permission_short_description);
            mWarningImage =(ImageView) view.findViewById(R.id.imageView_warning_app);
            mTextViewWarning=(TextView) view.findViewById(R.id.textView_warning);
            mLayout=(RelativeLayout) view.findViewById(R.id.relative_layout_permission);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mDescriptionView.getText() + "'";
        }
    }
}
