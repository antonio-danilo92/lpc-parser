package com.ssr.privacyguard.obj;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.util.SparseIntArray;

import com.ssr.privacyguard.MainApplication;
import com.ssr.privacyguard.R;
import com.ssr.privacyguard.utility.CategoryRiskCalculator;
import com.ssr.privacyguard.db.DBManager;
import com.ssr.privacyguard.utility.ImageUtilities;
import com.ssr.privacyguard.utility.MathUtility;
import com.ssr.privacyguard.utility.PermissionChecker;
import com.ssr.privacyguard.utility.RiskCalculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Antonio on 21/12/2016.
 */
public class Device {
    private static final String TAG = "Device";
    private static Device ourInstance;
    private ArrayList<Application> allApps;
    private double total;
    private int countAppThreshold;
    public static Device getInstance() {
        if(ourInstance==null)
            ourInstance=new Device();
        return ourInstance;
    }

    private Device() {
        allApps=getUserApps();
        Collections.sort(allApps, Collections.<Application>reverseOrder());
    }

    /**
     * invalidates the apps aqlready calulated. useful when you recreate the activity but the datas are still in memory (ex: change language)
     */
    public static void invalidate(){
        ourInstance=null;
    }
    //Call only user app
    public ArrayList<Application> getUserApps() {
        countAppThreshold=0;
        ArrayList<Application> currApps=new ArrayList<>();

        Context c = MainApplication.getAppContext();
        boolean systemApps=PreferenceManager.getDefaultSharedPreferences(c).getBoolean("system_apps",false);
        PackageManager packageManager = c.getPackageManager();
        List<ApplicationInfo> pkgAppsList = packageManager.getInstalledApplications(PackageManager.GET_META_DATA);
        double total = 0;
        if (pkgAppsList.isEmpty()) return currApps;
        for (ApplicationInfo resolveInfo : pkgAppsList) {
            if (systemApps || !isSystemApp(resolveInfo)) {
                PackageInfo packageInfo = null;
                PackageInfo servicesInfo = null;
                PackageInfo receiversInfo = null;

                try {
                    double score = 0;
                    Application currApp;
                    packageInfo = c.getPackageManager().getPackageInfo(resolveInfo.packageName, PackageManager.GET_PERMISSIONS);
                    servicesInfo = c.getPackageManager().getPackageInfo(resolveInfo.packageName, PackageManager.GET_SERVICES);
                    receiversInfo = c.getPackageManager().getPackageInfo(resolveInfo.packageName, PackageManager.GET_RECEIVERS);

                    Set<String> requestedPermissions;
                    if (packageInfo.requestedPermissions != null)
                        requestedPermissions = new HashSet<String>(Arrays.asList(packageInfo.requestedPermissions));
                    else
                        requestedPermissions = new HashSet<String>();

                    // get permissions required by app's services
                    Set<String> permissions=getServicesPermissions(servicesInfo);
                    if (permissions!=null)
                        requestedPermissions.addAll(permissions);

                    permissions=getReceivers(receiversInfo);
                    if (permissions!=null)
                        requestedPermissions.addAll(permissions);


                    if (!requestedPermissions.isEmpty()) {
                        List<String> allPermissions = new ArrayList<String>(requestedPermissions);
                        currApp= calculate(packageInfo.packageName, allPermissions);
                        currApps.add(currApp);
                        score = currApp.getPrivacyScore();
                        total = total + score;
                    }
                    //Log.d("Debug ", packageInfo.packageName+ "Score: "+score);
                    // text= text+ packageInfo.packageName+ "\nScore: "+score+"\n";
                } catch (PackageManager.NameNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            this.total = total;
        }
        return currApps;
    }

    // get permissions required by services
    private Set<String> getServicesPermissions(PackageInfo servicesInfo){
        ServiceInfo[] services=null;
        Set<String> requestedPermissions=new HashSet<String>();
        if (servicesInfo != null) {
            services = servicesInfo.services;
            if (services != null)
                for (ServiceInfo service : services) {
                    String permission = service.permission;
                    if (permission != null)
                        requestedPermissions.add(permission);
                }
        }
        return requestedPermissions;
    }

    private Set<String> getReceivers(PackageInfo receiversInfo) {
        ActivityInfo[] receivers = null;
        Set<String> requestedPermissions=new HashSet<String>();
        if (receiversInfo != null) {
            receivers = receiversInfo.receivers;
            if (receivers != null)
                for (ActivityInfo rec : receivers) {
                    String permission = rec.permission;
                    if (permission != null)
                        requestedPermissions.add(permission);
                }
        }
        return requestedPermissions;
    }

    private boolean isSystemApp(ApplicationInfo ai){
        return (ai.flags & ApplicationInfo.FLAG_SYSTEM) != 0;
    }

    //Call all app
    public synchronized void updateListOfApps() {
        ArrayList<Application> tmp= getUserApps();
        allApps.clear(); //update the main list of apps only after the retrival of all new apps. this avoid inconsistency states
        allApps.addAll(tmp);
    }

    private Application calculate(String packageName, List<String> permissionList){
        Context c=MainApplication.getAppContext();
        List<Permission> permissions= new ArrayList<>();
        for(String p: permissionList){
            Permission permission= DBManager.getPermissionByName(p);
            //check if permission is enabled
            if(permission!=null && PermissionChecker.checkPermission(permission, packageName))
                //if permission is  present in the db
                permissions.add(permission);

        }
        Application app= new Application(packageName, permissions);
        //change CategoryRiskCalculator instance to inertace RiskCalculator
        RiskCalculator calculator=new CategoryRiskCalculator();
        double score=calculator.calcRisk(app);
        score= addPenalty(app, score);
        Drawable icon;
        try {
            icon= c.getPackageManager().getApplicationIcon(packageName);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG,"Application named "+ packageName+" not found. I can't retrieve the app icon");
            icon= ResourcesCompat.getDrawable(c.getResources(),R.mipmap.ic_app_default,null);
        }
        app.setPrivacyScore(score);
        app.setAppIcon(ImageUtilities.drawableToBitmap(icon));
        app.setAppName(getApplicationName(packageName));
        //check threshold
        if(score>MainApplication.getAppContext().getResources().getInteger(R.integer.threshold_red))
            countAppThreshold++;
        return app;
    }

    /**
     * for each application this method add to the score the releated penalties
     * @param app
     * @param score
     * @return score +penalty
     */

    private double addPenalty(Application app, double score){
        SparseIntArray countSubcatgory= new SparseIntArray();
        countSubcatgory.put(Permission.A, 0);
        countSubcatgory.put(Permission.B, 0);
        countSubcatgory.put(Permission.C, 0);
        countSubcatgory.put(Permission.D, 0);
        countSubcatgory.put(Permission.E, 0);
        countSubcatgory.put(Permission.F, 0);
        countSubcatgory.put(Permission.G, 0);
        countSubcatgory.put(Permission.H, 0);
        countSubcatgory.put(Permission.SPECIAL, 0);
        //collect all permissions
        for(Permission p: app.getPermissionList())
            countSubcatgory.put(p.getSubCategory(),countSubcatgory.get(p.getSubCategory())+1);
        //check for penalties
        if(countSubcatgory.get(Permission.A)>0) {
            if (countSubcatgory.get(Permission.C) > 0 && countSubcatgory.get(Permission.F) > 0)
                score = score * MathUtility.ACF;
            if (countSubcatgory.get(Permission.C) > 0 && countSubcatgory.get(Permission.G) > 0)
                score = score * MathUtility.ACG;
            if (countSubcatgory.get(Permission.C) > 0 && countSubcatgory.get(Permission.H) > 0)
                score = score * MathUtility.ACH;
            if (countSubcatgory.get(Permission.E) > 0 && countSubcatgory.get(Permission.F) > 0)
                score = score * MathUtility.AEF;
            if (countSubcatgory.get(Permission.E) > 0 && countSubcatgory.get(Permission.G) > 0)
                score = score * MathUtility.AEG;
            if (countSubcatgory.get(Permission.E) > 0 && countSubcatgory.get(Permission.H) > 0)
                score = score * MathUtility.AEH;
        }
        if(countSubcatgory.get(Permission.B)>0) {
            if (countSubcatgory.get(Permission.C) > 0 && countSubcatgory.get(Permission.F) > 0)
                score = score * MathUtility.BCF;
            if (countSubcatgory.get(Permission.C) > 0 && countSubcatgory.get(Permission.G) > 0)
                score = score * MathUtility.BCG;
            if (countSubcatgory.get(Permission.C) > 0 && countSubcatgory.get(Permission.H) > 0)
                score = score * MathUtility.BCH;
            if (countSubcatgory.get(Permission.E) > 0 && countSubcatgory.get(Permission.F) > 0)
                score = score * MathUtility.BEF;
            if (countSubcatgory.get(Permission.E) > 0 && countSubcatgory.get(Permission.G) > 0)
                score = score * MathUtility.BEG;
            if (countSubcatgory.get(Permission.E) > 0 && countSubcatgory.get(Permission.H) > 0)
                score = score * MathUtility.BEH;
        }
        //check if score is greater then 100 after penalties
        if(score>100) return 100;
        else return score;
    }

    private static String getApplicationName(String packageName) {
        final PackageManager pm = MainApplication.getAppContext().getPackageManager();
        ApplicationInfo ai;
        try {
            ai = pm.getApplicationInfo( packageName, 0);
        } catch (final PackageManager.NameNotFoundException e) {
            ai = null;
        }
        return (String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)");
    }

    //this metod return the number of apps above the threshold of dangerousness
    public int getCountAppThreshold(){return countAppThreshold;}

    public synchronized ArrayList<Application> getAllApps() {
        return allApps;
    }
}
