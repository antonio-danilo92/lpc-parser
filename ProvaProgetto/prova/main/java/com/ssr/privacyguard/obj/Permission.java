package com.ssr.privacyguard.obj;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Comparator;



/**
 * Created by Bitto on 05/12/2016.
 */

public class Permission implements Parcelable,Comparable<Permission> {
    public final static int HARDWARE_ACCESS=1;
    public final static int DATA_ACCESS=2;
    public final static int COMMUNICATION=3;
    public final static int SYSTEM=4;
    public final static int A=1;
    public final static int B=2;
    public final static int C=3;
    public final static int D=4;
    public final static int E=5;
    public final static int F=6;
    public final static int G=7;
    public final static int H=8;
    public final static int SPECIAL=9;
    private String permissionName;
    private int category;
    private int subCategory;
    private int punteggio;
    private String warning;
    private String description;
    private int imageId;

    public Permission(String permName, String description, String warning, int category, int subCategory, int score) {
        this.category = category;
        this.permissionName = permName;
        this.punteggio = score;
        this.description=description;
        this.warning=warning;
        this.subCategory = subCategory;
        this.imageId=0;
    }

    public Permission(String permName, String description, String warning, int category, int subCategory, int score, int imageID) {
        this.category = category;
        this.permissionName = permName;
        this.punteggio = score;
        this.description=description;
        this.warning=warning;
        this.subCategory = subCategory;
        this.imageId=imageID;
    }

    public void setWarning(String warning) {
        this.warning = warning;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public int getPunteggio() {
        return punteggio;
    }

    public void setPunteggio(int punteggio) {
        this.punteggio = punteggio;
    }

    public int getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(int subCategory) {
        this.subCategory = subCategory;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "category=" + category +
                ", permissionName='" + permissionName + '\'' +
                ", subCategory=" + subCategory +
                ", punteggio=" + punteggio +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.permissionName);
        dest.writeInt(this.category);
        dest.writeInt(this.subCategory);
        dest.writeInt(this.punteggio);
    }

    protected Permission(Parcel in) {
        this.permissionName = in.readString();
        this.category = in.readInt();
        this.subCategory = in.readInt();
        this.punteggio = in.readInt();
    }

    public static final Parcelable.Creator<Permission> CREATOR = new Parcelable.Creator<Permission>() {
        @Override
        public Permission createFromParcel(Parcel source) {
            return new Permission(source);
        }

        @Override
        public Permission[] newArray(int size) {
            return new Permission[size];
        }
    };

    public String getWarning() {
        return warning;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public int compareTo(Permission o) {
        //order by decrising score
        int x=this.punteggio;
        int y=o.getPunteggio();
        int cmp=  (x < y) ? 1 : ((x == y) ? 0 : -1);
        //if same score, alfabetical order
        if(cmp==0) return this.permissionName.compareTo(o.getPermissionName());
        else return cmp;

    }
    public int getImageId() {
        return imageId;
    }

    public static final class ScoreComparator implements Comparator<Permission> {

        @Override
        public int compare(Permission o1, Permission o2) {
            int x=o1.getPunteggio();
            int y=o2.getPunteggio();
            return (x < y) ? 1 : ((x == y) ? 0 : -1);
        }

        @Override
        public boolean equals(Object obj) {
            return false;
        }


    }

}
