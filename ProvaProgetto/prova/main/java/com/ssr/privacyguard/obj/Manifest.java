package com.ssr.privacyguard.obj;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bitto on 05/12/2016.
 */

public class Manifest implements Parcelable {

    private List<Permission> permissionList;
    private String packageName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Manifest manifest = (Manifest) o;

        return packageName.equals(manifest.packageName);

    }

    @Override
    public int hashCode() {
        return packageName.hashCode();
    }


    public Manifest(String packageName, List<Permission> permissionArrayList) {
        this.permissionList = permissionArrayList;
        this.packageName = packageName;
    }

    public Manifest(String packageName) {
        this.permissionList = new ArrayList<>();
        this.packageName = packageName;
    }

    public static final Creator<Manifest> CREATOR = new Creator<Manifest>() {
        @Override
        public Manifest createFromParcel(Parcel in) {
            return new Manifest(in);
        }

        @Override
        public Manifest[] newArray(int size) {
            return new Manifest[size];
        }
    };

    public List<Permission> getPermissionList() {
        return permissionList;
    }

    public void setPermissionArrayList(ArrayList<Permission> permissionArrayList) {
        this.permissionList = permissionArrayList;
    }

    public void addPermission(Permission nP){
        permissionList.add(nP);
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    @Override
    public String toString() {
        return "Manifest{" +
                "packageName='" + packageName + '\'' +
                ", permissionList=" + permissionList +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.permissionList);
        dest.writeString(this.packageName);
    }

    protected Manifest(Parcel in) {
        this.permissionList = new ArrayList<>();
        in.readList(this.permissionList, Permission.class.getClassLoader());
        this.packageName = in.readString();
    }

}
