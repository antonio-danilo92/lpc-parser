package com.ssr.privacyguard.obj;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Antonio on 21/12/2016.
 */

public class Application extends Manifest implements Comparable<Application> {
    private double privacyScore;
    private Bitmap appIcon;
    private String appName;

    public Application(String packageName, List<Permission> permissionArrayList) {
        super(packageName, permissionArrayList);
    }

    public Application(String appName) {
        super(appName);
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public double getPrivacyScore() {
        return privacyScore;
    }

    public void setPrivacyScore(double privacyScore) {
        this.privacyScore = privacyScore;
    }

    public Bitmap getAppIcon() {
        return appIcon;
    }

    public void setAppIcon(Bitmap appIcon) {
        this.appIcon = appIcon;
    }

    @Override
    public String toString() {
        return "Application{" +
                ", appName= "+ getPackageName()+
                "privacyScore=" + privacyScore +
                '}';
    }

    @Override
    public int compareTo(@NonNull Application toCompare) {
        if(privacyScore<toCompare.getPrivacyScore()) return -1;
        else if (privacyScore>toCompare.getPrivacyScore())return 1;
        else return 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeDouble(this.privacyScore);
        dest.writeParcelable(this.appIcon, flags);
        dest.writeString(this.appName);
     /*   if(isWarning())
            dest.writeString(this.warning);
    */
    }

    protected Application(Parcel in) {
        super(in);
        this.privacyScore = in.readDouble();
        this.appIcon = in.readParcelable(Bitmap.class.getClassLoader());
        this.appName = in.readString();
    }

    public static final Creator<Application> CREATOR = new Creator<Application>() {
        @Override
        public Application createFromParcel(Parcel source) {
            return new Application(source);
        }

        @Override
        public Application[] newArray(int size) {
            return new Application[size];
        }
    };
}
