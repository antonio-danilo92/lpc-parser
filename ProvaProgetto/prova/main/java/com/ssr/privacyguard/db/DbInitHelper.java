package com.ssr.privacyguard.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class DbInitHelper extends SQLiteAssetHelper {
    private static final int DATABASE_VERSION = 7;


    public DbInitHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        setForcedUpgrade();
    }
}
