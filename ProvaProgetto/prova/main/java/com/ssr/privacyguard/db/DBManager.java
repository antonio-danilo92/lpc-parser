package com.ssr.privacyguard.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ssr.privacyguard.MainApplication;
import com.ssr.privacyguard.obj.Permission;

import java.util.Locale;

/**
 * Created by Assunta on 08/12/2016.
 */

public class DBManager {
    private static SQLiteDatabase db;
    public static void initDB() {
        if (db == null) {
            DbInitHelper d = new DbInitHelper(MainApplication.getAppContext());
            db = d.getWritableDatabase();
        }
    }

    public static Permission getPermissionByName(String name){
        String language=Locale.getDefault().getLanguage();
        if(db==null) initDB();
        String args[]={name};
        Cursor c=db.query("permissions",null,"permissionName=?",args,null,null,"permissionName");
        if(c.getCount()==0) {
            c.close();
            return null;
        }
        c.moveToFirst();
        String permName=c.getString(0).trim();
        int descrIndex=c.getColumnIndex("description-"+language);
        String description;
        if(descrIndex==-1)
          description=c.getString(1).trim();
        else
            description=c.getString(descrIndex);
        int warnIndex=c.getColumnIndex("warning-"+language);
        String warning;
        if(warnIndex==-1)
            warning=c.getString(2);
        else
            warning=c.getString(warnIndex);
        if(warning!=null)
            warning=warning.trim();
        int category=c.getInt(3);
        int subCategory=c.getInt(4);
        int score=c.getInt(5);
        int imageID=c.getInt(6);
        c.close();
        return new Permission(permName,description,warning,category,subCategory,score,imageID);
    }
}
