package com.ssr.privacyguard.fragments;

import android.content.Context;
import android.media.Image;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ssr.privacyguard.R;
import com.ssr.privacyguard.adapters.PermissionRecyclerViewAdapter;
import com.ssr.privacyguard.adapters.SimpleSectionedRecyclerViewAdapter;
import com.ssr.privacyguard.customViews.WaveView;
import com.ssr.privacyguard.obj.Application;
import com.ssr.privacyguard.obj.Permission;
import com.ssr.privacyguard.utility.MathUtility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

public class FragmentPermission extends Fragment {

    private static final String APPLICATION = "selectedApp";
    // TODO: Customize parameters
    private Application mSelectedApplication;
    private int red;
    private int yellow;
    private int green;


    public FragmentPermission() {
    }


    public static FragmentPermission newInstance(Application app) {
        FragmentPermission fragment = new FragmentPermission();
        Bundle args = new Bundle();
        args.putParcelable(APPLICATION, app);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onResume() {
        super.onResume();
        TextView tv= (TextView) getActivity().findViewById(R.id.num_score);
        double privacyScore = mSelectedApplication.getPrivacyScore();
        tv.setText(String.valueOf(MathUtility.round(privacyScore,2)));
        ImageView iv=(ImageView) getActivity().findViewById(R.id.app_img);
        iv.setImageBitmap(mSelectedApplication.getAppIcon());
        WaveView wv=(WaveView) getActivity().findViewById(R.id.wave_view);
        if(privacyScore>getResources().getInteger(R.integer.threshold_red)) {

            wv.setBackgroundColor(red);
        }
        else if(privacyScore>getResources().getInteger(R.integer.threshold_yellow)) {
            wv.setBackgroundColor(yellow);
        }
        else {
            wv.setBackgroundColor(green);
        }

        wv.setProgress((int) privacyScore);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_permission_list, container, false);
        red = ContextCompat.getColor(getContext(), R.color.md_red_500);
        yellow = ContextCompat.getColor(getContext(), R.color.md_yellow_800);
        green = ContextCompat.getColor(getContext(), R.color.md_green_500);
        //retrive selected application
        if (getArguments() != null) {
            mSelectedApplication = getArguments().getParcelable(APPLICATION);
        }

        // Set the adapter
        Context context = view.getContext();
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager=new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        List<Permission> permissionList = mSelectedApplication.getPermissionList();
        Collections.sort(permissionList);
        PermissionRecyclerViewAdapter adapter = new PermissionRecyclerViewAdapter(permissionList);

        List<SimpleSectionedRecyclerViewAdapter.Section> sections=getSeparator(permissionList);
        SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
        SimpleSectionedRecyclerViewAdapter mSectionedAdapter = new
                SimpleSectionedRecyclerViewAdapter(getContext(),R.layout.section,R.id.section_text,adapter);
        mSectionedAdapter.setSections(sections.toArray(dummy));
        AlphaInAnimationAdapter animatedAdapter= new AlphaInAnimationAdapter(mSectionedAdapter); //add anmimation to the adapter
        animatedAdapter.setFirstOnly(false);
        animatedAdapter.setDuration(200);
        recyclerView.setAdapter(animatedAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int scrollDy = 0;
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                scrollDy += dy;
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(scrollDy==0&&(newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE))
                {
                    AppBarLayout appBarLayout = ((AppBarLayout) getActivity().findViewById(R.id.bar));

                    appBarLayout.setExpanded(true);
                }
            }
        });
        return view;
    }

    /**
     * Search positions to insert separators. linear search to find the first item with a score greater than 7
     * the first item with a score greater than 4 but lower or equals than 7
     * and the first item with a score lower or equals then 4.
     * @param permissionList the permission list of the analize app
     * @return a list of at most 3 separators
     */
    private List<SimpleSectionedRecyclerViewAdapter.Section> getSeparator(List<Permission> permissionList) {
        List<SimpleSectionedRecyclerViewAdapter.Section> sections=new ArrayList<>();
        boolean mediumToSearch=true;
        Permission p;
        if(permissionList.isEmpty())
            return sections;
        if(permissionList.get(0).getPunteggio()>7)
            sections.add(new SimpleSectionedRecyclerViewAdapter.Section(0,getString(R.string.high_risk),red));
        for(int i=1;i<permissionList.size();i++){ //start from 1, because we have already tested the first item
            p=permissionList.get(i);
            if(mediumToSearch && p.getPunteggio()>4 && p.getPunteggio()<=7) {
                sections.add(new SimpleSectionedRecyclerViewAdapter.Section(i, getString(R.string.medium_risk),yellow));
                mediumToSearch=false;
            }
            else if(p.getPunteggio()<=4) {
                sections.add(new SimpleSectionedRecyclerViewAdapter.Section(i, getString(R.string.low_risk),green));
                return sections;
            }

        }
        return sections;
    }


}
