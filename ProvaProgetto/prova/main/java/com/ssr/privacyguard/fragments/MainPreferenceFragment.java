package com.ssr.privacyguard.fragments;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ssr.privacyguard.R;

/**
 * Created by Antonio on 10/02/2017.
 */

public class MainPreferenceFragment extends PreferenceFragmentCompat {
    private ListPreference mListPreference;


    @Override
    public void onResume() {
        super.onResume();
        getListView().setNestedScrollingEnabled(false);

    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.main_preferences,rootKey);
    }

}
