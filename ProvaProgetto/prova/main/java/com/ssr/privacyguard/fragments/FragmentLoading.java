package com.ssr.privacyguard.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ssr.privacyguard.R;
import com.ssr.privacyguard.obj.Device;

import az.plainpie.PieView;

/**
 * Created by Assunta on 26/12/2016.
 */

public class FragmentLoading extends Fragment {

    public static final String TAG = "FragmentLoading";


    public FragmentLoading() {

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_loading, container, false);
        return view;
    }
}