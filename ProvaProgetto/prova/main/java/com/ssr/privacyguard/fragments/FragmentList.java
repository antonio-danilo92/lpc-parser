package com.ssr.privacyguard.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ssr.privacyguard.BuildConfig;
import com.ssr.privacyguard.activities.MainActivity;
import com.ssr.privacyguard.adapters.AdapterApp;
import com.ssr.privacyguard.R;
import com.ssr.privacyguard.obj.Application;
import com.ssr.privacyguard.obj.Device;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

import static com.ssr.privacyguard.R.id.recycler_list_app;

public class FragmentList extends Fragment{

    private static final String TAG = "FragmentList";
    private OnListFragmentInteractionListener mListener;
    private LinearLayoutManager llm;
    private static int index=0;
    private static int top=0;
    private static boolean needToRefresh=false;
    private AdapterApp mAdapter;
    private ArrayList<Application> mApps;
    private SwipeRefreshLayout swipeRefreshLayout;//swiperefresh
    private SharedPreferences prefs;
    private Context mContext;
    private SharedPreferences.OnSharedPreferenceChangeListener listener; //needed as a workaround for an android bug

    public FragmentList() {

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        mContext=view.getContext();
        //recyclerview
        RecyclerView recyclerView = (RecyclerView) view.findViewById(recycler_list_app);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false); //disable expandable toolbar
        llm = new LinearLayoutManager(mContext);
        //get the list of all apps
        mApps= Device.getInstance().getAllApps();
        prefs=PreferenceManager.getDefaultSharedPreferences(mContext);
        sortApps();

        //Refresh list
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.main_swipe);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(mContext,R.color.colorPrimary));

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh()
            {
                new RefreshTask().execute();
            }
        });

        mAdapter=new AdapterApp(mApps, new OnListFragmentInteractionListener() {
            @Override
            public void onListFragmentInteraction(Application item) {
                //save position of the list when you tap on an element
                index = llm.findFirstVisibleItemPosition();
                View v = llm.getChildAt(0);
                top = (v == null) ? 0 : (v.getTop() - llm.getPaddingTop());
                //continue the chain call of the listeners (in this case, the activity takes control)
                mListener.onListFragmentInteraction(item);
            }
        });
        mAdapter.setHasStableIds(true);
        AlphaInAnimationAdapter animatedAdapter= new AlphaInAnimationAdapter(mAdapter); //add anmimation to the adapter
        animatedAdapter.setDuration(100);

        animatedAdapter.setFirstOnly(false);
        // animatedAdapter.setInterpolator(new OvershootInterpolator());
        recyclerView.setAdapter(animatedAdapter);
        recyclerView.setLayoutManager(llm);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                llm.getOrientation());

        recyclerView.addItemDecoration(dividerItemDecoration);

        // Use instance field for listener
        // It will not be gc'd as long as this instance is kept referenced
        listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                if(key.equals("system_apps")) {
                    needToRefresh=true;
                }
            }
        };
        prefs.registerOnSharedPreferenceChangeListener(listener);
        return view;
    }

    private void sortApps() {
        String sortType= prefs.getString("SORT_TYPE", "SCORE");
        //check type of sort, default SCORE
        if(sortType.equalsIgnoreCase("NAME"))
            mApps=sortName(mApps);
            //"else if" not necessary, used to avoid regression errors
        else if(sortType.equalsIgnoreCase("SCORE")){
            mApps=sortScore(mApps);

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        llm.scrollToPositionWithOffset(index,top);
        //check if you need to refresh items
        if(needToRefresh){
            needToRefresh=false;
            swipeRefreshLayout.setRefreshing(true);
            new RefreshTask().execute();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.

        inflater.inflate(R.menu.menu_scrolling, menu);

        //check kind of sort to cheched the correct radio button
        String sortType =prefs.getString("SORT_TYPE", getString(R.string.sort_score_value));
        if(sortType.equals(getString(R.string.sort_score_value)))
            menu.getItem(0).getSubMenu().getItem(1).setChecked(true);  //the menu group handling the choice is a subMenu. the group than contains two other items: sort by sco9re and sort by name
        else if(sortType.equals(getString(R.string.sort_name_value)))
            menu.getItem(0).getSubMenu().getItem(0).setChecked(true);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id==R.id.action_order_by_name){
            prefs.edit().putString("SORT_TYPE", "NAME").apply();

            sortName(mApps);
            mAdapter.notifyDataSetChanged();
            item.setChecked(true);
            return true;
        }
        if (id==R.id.action_order_by_score){
            prefs.edit().putString("SORT_TYPE", "SCORE").apply();
            sortScore(mApps);
            mAdapter.notifyDataSetChanged();
            item.setChecked(true);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<Application>  sortScore(ArrayList<Application> mApps) {
        //sort list application by name
        Collections.sort(mApps, new Comparator<Application>() {
            @Override
            public int compare(Application o1, Application o2) {
                return Double.compare(o2.getPrivacyScore(),o1.getPrivacyScore()); //from highest to lowest score
            }
        });
        return mApps;
    }

    private ArrayList<Application> sortName(ArrayList<Application> apps){
        //sort list application by name
        Collections.sort(apps, new Comparator<Application>() {
            @Override
            public int compare(Application o1, Application o2) {
                return o1.getAppName().toUpperCase().compareTo(o2.getAppName().toUpperCase());
            }
        });
        return apps;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Application item);
    }

    private class RefreshTask extends AsyncTask<Object, Object, ArrayList<Application>> {
        @Override
        protected void onPostExecute(ArrayList<Application> p) {
            mApps.clear();
            mApps.addAll(p);
            sortApps();
            //


            if (Device.getInstance().getAllApps()!=mApps)
                Log.e("ERROR", "apps contained in device and apps contained in fragmentlist are not synced");

            mAdapter.notifyDataSetChanged();

            updateFragmentScore();
            // Call setRefreshing(false) when the list has been refreshed.
            swipeRefreshLayout.setRefreshing(false);
        }

        @Override
        protected ArrayList<Application> doInBackground(Object... params) {
            return Device.getInstance().getUserApps();
        }
    }

    /**
     * method to update fragmentScore after updating the list
     */
    private void updateFragmentScore(){
        MainActivity main=(MainActivity)mContext;
        if (main.findViewById(R.id.fragment_container_score) != null) {
            FragmentScore fragmentScore = (FragmentScore) main.getSupportFragmentManager().findFragmentByTag(FragmentScore.TAG);
            if(fragmentScore!=null)
                fragmentScore.setMessage();

        }
    }
}
