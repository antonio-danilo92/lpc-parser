package com.ssr.privacyguard.fragments;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ssr.privacyguard.MainApplication;
import com.ssr.privacyguard.R;
import com.ssr.privacyguard.activities.MainActivity;
import com.ssr.privacyguard.obj.Device;

import az.plainpie.PieView;

/*
* This fragment display a message and a bugdroid to synthetize
* the data leakage risk
 */
public class FragmentScore extends Fragment {
    public static String TAG="FRAGMENT_SCORE";
    private TextView textView;
    private ImageView imageView;

    public FragmentScore() {

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_score_android, container, false);
        textView= (TextView) view.findViewById(R.id.textView_score);
        imageView=(ImageView) view.findViewById(R.id.imageView_android);
        setMessage();
        return view;
    }

    public void setMessage(){
        int numApp=Device.getInstance().getCountAppThreshold();

        if(numApp>0) {
            imageView.setImageResource(R.mipmap.ic_android_red);
            textView.setText(getResources().getQuantityString(R.plurals.fragment_score_high_risk,numApp, numApp));
        }
        else {
            imageView.setImageResource(R.mipmap.ic_android);
            textView.setText(getResources().getString(R.string.no_threshold));
        }
    }
}
