package com.ssr.privacyguard.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import com.ssr.privacyguard.R;
import com.vansuita.materialabout.builder.AboutBuilder;

public class AboutActivity extends AppCompatActivity {

    /**
     * Create dinamically an about screen using the material About library
     * @param savedInstanceState not used
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        final FrameLayout flHolder = (FrameLayout) findViewById(R.id.about_frame);
        PackageInfo pInfo = null;
        String version;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            //impossible scenario
            version="";
        }
        View view = AboutBuilder.with(this)
                .setPhoto(R.mipmap.ic_launcher_rectangular)
                .setCover(R.mipmap.profile_cover)
                .setIconColor(Color.BLACK)

                .setName(R.string.app_name)
                .setSubTitle(version)
                .setBrief(R.string.description_about)
                .setAppName(R.string.contact)
                .addLink(com.vansuita.materialabout.R.mipmap.google_play_store, com.vansuita.materialabout.R.string.google_play_store, Uri.parse(getString(R.string.play_store_link)))
                .addLink(R.mipmap.ic_iswat_logo,"IswatLab",Uri.parse("http://www.iswatlab.eu/"))
                .setLinksColumnsCount(2)
                .addAction(R.drawable.ic_action_mail, getResources().getString(R.string.developer_contact_Antonio),new sendEmailListener(getResources().getString(R.string.antonio_email)))
                .addAction(R.drawable.ic_action_mail, getResources().getString(R.string.developer_contact_Fabrizio),new sendEmailListener(getResources().getString(R.string.fabrizio_email)))
                .addAction(R.drawable.ic_action_mail, getResources().getString(R.string.developer_contact_Alfredo),new sendEmailListener(getResources().getString(R.string.alfredo_email)))
                .addAction(R.drawable.ic_action_mail, getResources().getString(R.string.developer_contact_Assunta),new sendEmailListener(getResources().getString(R.string.assunta_email)))
                .addFeedbackAction("nago.devel@gmail.com")

                .addAction(R.drawable.ic_action_mail, "Supervisor: Corrado Aaron Visaggio",new sendEmailListener(getResources().getString(R.string.visaggio_email)))
                .addFiveStarsAction("com.ssr.privacyguard")
                .addShareAction(R.string.app_name)
                .addDonateAction(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url = "https://ko-fi.com/A501M8G";
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
                    }
                })

                .build();

        flHolder.addView(view);
    }

    /**
     * A listener used after tapping on an email button.
     * It let the user choose an application to handle the mail sending
     */
    private class sendEmailListener implements View.OnClickListener{
        private String email;

        /**
         * Constructor
         * @param email address to use as recipient
         */
        sendEmailListener(String email){
            this.email=email;
        }

        /**
         * called after tapping on an email button
         * @param v: the tapped view
         */
        @Override
        public void onClick(View v) {

            Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"+email));
            startActivity(intent);
        }
    }

}