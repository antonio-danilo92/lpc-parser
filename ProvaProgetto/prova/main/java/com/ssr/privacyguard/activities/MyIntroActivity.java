package com.ssr.privacyguard.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;
import com.ssr.privacyguard.R;

public class MyIntroActivity extends IntroActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        /* Enable/disable fullscreen */
        setFullscreen(true);
        /* Enable/disable finish button */
        setFinishEnabled(true);
        /**
         *create first silde
         */
        addSlide(new SimpleSlide.Builder()
                .title(R.string.tutorial_title)
                .description(R.string.description_about)
                .image(R.mipmap.ic_launcher)
                .background(R.color.colorPrimary)
                .backgroundDark(R.color.colorPrimary)
                .build());

    }


}
