package com.ssr.privacyguard.activities;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.transition.Slide;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.github.akashandroid90.imageletter.MaterialLetterIcon;
import com.github.aleksandermielczarek.hamburgerarrownavigation.HamburgerArrowNavigation;
import com.github.aleksandermielczarek.hamburgerarrownavigation.HamburgerArrowNavigator;
import com.ssr.privacyguard.R;
import com.ssr.privacyguard.fragments.FragmentList;
import com.ssr.privacyguard.fragments.FragmentLoading;
import com.ssr.privacyguard.fragments.FragmentPermission;
import com.ssr.privacyguard.fragments.FragmentScore;
import com.ssr.privacyguard.fragments.MainPreferenceFragment;
import com.ssr.privacyguard.obj.Application;
import com.ssr.privacyguard.obj.Device;

import java.util.ArrayList;
import java.util.List;

import angtrim.com.fivestarslibrary.FiveStarsDialog;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FragmentList.OnListFragmentInteractionListener {
    // TODO: Customize parameter argument names
    public static final String SECOND_SCREEN_FRAGMENT_TAG = "secondFrag";
    private final int MY_PERMISSIONS_GET_ACCOUNTS = 0;
    private final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 1;
    private NavigationView navigationView;
    private CollapsingToolbarLayout mCtl;
    private Toolbar mToolbar;
    private AppBarLayout mAppBarLayout;
    private ActionBarDrawerToggle mActionBarToggle;
    private HamburgerArrowNavigator hamburgerArrowNavigator;
    private InitializeDeviceTask initializeDeviceTask;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Device.invalidate();  // invalidate all the previously analyzed app. new activity -> new analysis
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout); //navigation drawer
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED); //disable navigation drawer


        //Set Loading Fragment
        if (findViewById(R.id.fragment_container_score) != null) {
            // Create a new LoadingFragment to be placed in the activity layout
            FragmentLoading fragmentLoading = new FragmentLoading();
            // Add the fragment to the 'fragment_container_list' FrameLayout. the bottom fragment
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container_list, fragmentLoading, FragmentLoading.TAG).commit();
        }
        setAppBarDragging(false); //disable dragging on the toolbar. The collpsible toolbar won't open dragging on the toolbar. It will be open only if you sroll through the list
        //New Permission system
        if (Build.VERSION.SDK_INT >= 23) {
            //request the required permissions
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.GET_ACCOUNTS}, MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
            checkForTutorial();
        }else {
            checkForTutorial();
            //start the apps analysis
            initializeDeviceTask = new InitializeDeviceTask();
            initializeDeviceTask.execute(savedInstanceState);
        }
    }

    //start tutorial if first time
    private void checkForTutorial(){
        SharedPreferences settings = getSharedPreferences("PREFERENCES", 0);
        if (settings.getBoolean("first_time", true)) {
            settings.edit().putBoolean("first_time", false).commit();
            Intent intent = new Intent(MainActivity.this, MyIntroActivity.class);
            startActivity(intent);
        }

    }

    /**
     * Expand or collapse the toolbar
     * @param expanded expand the toolbar if true
     * @param animate use an animation during the expansion if true
     */
    private void setToolbarExpanded(boolean expanded, boolean animate) {
        if (mAppBarLayout == null)
            mAppBarLayout = (AppBarLayout) findViewById(R.id.bar);
        mAppBarLayout.setExpanded(expanded, animate);
    }

    /**
     * Set the toolbar evelvation. the elevation is used by android to calculate a shadow
     * above the view.
     * @param dp height of the toolbar
     */
    private void setToolbarElevation(int dp) {
        if (mAppBarLayout == null)
            mAppBarLayout = (AppBarLayout) findViewById(R.id.bar);
        ViewCompat.setElevation(mAppBarLayout, dp);
    }

    /**
     * initialize the standard toolbar and navigation drawer.
     */
    //TODO too long. refactoring needed
    private void setupDefaultToolbar() {
        if (mToolbar == null) { //don't reinitialize the toolbar if exists.
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(mToolbar);
            ActionBar ab = getSupportActionBar();
            if (ab != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayShowTitleEnabled(false); //disable automatic title on toolbar. we will setup it manually
            }

            setAppBarDragging(false); //disable app bar dragging
            setToolbarTitle(getResources().getString(R.string.app_name));

        }
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setDrawerLockMode(
                DrawerLayout.LOCK_MODE_UNLOCKED);

        mActionBarToggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close); //initialize hamburger
        setToolbarBackIcon(); //setup the animated hamburger icon

        mActionBarToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hamburgerArrowNavigator.isArrow())
                    onBackPressed(); //if is showed an arrow, go back
                else
                    drawer.openDrawer(GravityCompat.START); //if is showed an hamburger, open the drawer tapping on it
            }
        });

        drawer.addDrawerListener(mActionBarToggle);
        mActionBarToggle.syncState();
        if (navigationView == null)
            navigationView = (NavigationView) findViewById(R.id.nav_view);
        setNavigationHeader(navigationView);
        navigationView.setNavigationItemSelectedListener(this);
    }

    protected void onSaveInstanceState(Bundle outState) {
        //No call for super(). Bug on API Level > 11.
    }

    /**
     * set the first scren, after the loading screen
     * @param savedInstanceState currently not used
     */
    private void setFragmentMasterScreen(Bundle savedInstanceState) {
        if (hamburgerArrowNavigator == null)
            setToolbarBackIcon();
        hamburgerArrowNavigator.animateToHamburger(); //is the first screeen, so we need an hambuirger
        setTopMasterFragment(savedInstanceState); //set the fragment on the top, showing the "total score"
        setBottomMasterFragment(savedInstanceState); //set the fragment on the bottom, showing all the apps
        //add rete dialog
        rateDialog();
    }

    /**
     * this method add a rete dialog to rate the application
     * we can set listener for negative review or all review
     */
    private void rateDialog() {
        FiveStarsDialog fiveStarsDialog = new FiveStarsDialog(this, "nago.devel@gmail.com");
        fiveStarsDialog.setRateText(getResources().getString(R.string.rate_description))
                .setTitle(getResources().getString(R.string.rate_title))
                .setForceMode(false)
                .setUpperBound(2) // Market opened if a rating >= 2 is selected
                .showAfter(6);
        //if we want we can decide to recive emails for negative reviews or all reviews
                /*fiveStarsDialog.setNegativeReviewListener(new NegativeReviewListener() {
                    @Override
                    public void onNegativeReview(int i) {

                    }// OVERRIDE mail intent for negative review
                }); */
                /*fiveStarsDialog.setReviewListener(new ReviewListener() {
                    @Override
                    public void onReview(int i) {

                    }
                }); // Used to listen for reviews (if you want to track them )*/
    }

    /**
     * set the bottom fragment to the list of apps
     * @param savedInstanceState currently not used
     */
    private void setBottomMasterFragment(Bundle savedInstanceState) {
        if (findViewById(R.id.fragment_container_list) != null) {
            FragmentList fragmentList = new FragmentList();
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                fragmentList.setEnterTransition(new Slide(Gravity.END)); //enter animation
            }
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container_list, fragmentList).commitAllowingStateLoss();
        }
    }

    /**
     * set the top fragment to the "total score" of the device
     * @param savedInstanceState currently not used
     */
    private void setTopMasterFragment(Bundle savedInstanceState) {
        //Set Fragments
        if (findViewById(R.id.fragment_container_score) != null) {
            if (savedInstanceState != null) {
                return;
            }
            FragmentScore fragmentScore = new FragmentScore();
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                fragmentScore.setEnterTransition(new Slide(Gravity.END)); // enter animation
            }

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container_score, fragmentScore, FragmentScore.TAG).commitAllowingStateLoss();
        }
    }

    /**
     *  Enable or disable the AppBar drag
     *  by deafult if you drag the collapsible toolbar, it will expand
     *  if you disable it, it will expand only if you scroll through the list
     * @param isDragging enable or siable the app bar dragging
     */
    private void setAppBarDragging(final boolean isDragging) {
        if (mAppBarLayout == null)
            mAppBarLayout = (AppBarLayout) findViewById(R.id.bar);
        CoordinatorLayout.LayoutParams params =
                (CoordinatorLayout.LayoutParams) mAppBarLayout.getLayoutParams();
        AppBarLayout.Behavior behavior = new AppBarLayout.Behavior();
        behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
            @Override
            public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                return isDragging;
            }
        });
        params.setBehavior(behavior);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START); //if drawer is opened, then close it
        } else {
            Fragment fp = getSupportFragmentManager().findFragmentByTag(SECOND_SCREEN_FRAGMENT_TAG);
            if (fp != null && fp.isVisible()) { // if you are in a secondary screen (settings or app details)
                setToolbarExpanded(false, true); //close the collapsible toolbar
                setToolbarTitle(getResources().getString(R.string.app_name)); //set the toolbar tiotle to the app name
                this.setFragmentMasterScreen(null); //set the all apps screen
            } else
                super.onBackPressed(); // if you're in the fiorst screeen, than close the app
        }
    }

    /**
     * set the toolbar title to the provided string
     * @param title toolbar title to set
     */
    private void setToolbarTitle(String title) {
        if (mCtl == null)
            mCtl = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        mCtl.setTitle(title);
    }

    /**
     * initialize the animated hamburger icon
     */
    private void setToolbarBackIcon() {
        if (mActionBarToggle == null)
            setupDefaultToolbar(); //if the toolbar is not already initialized, initialize it
        mActionBarToggle.setDrawerIndicatorEnabled(false); //disable the default drawer hamburger
        //build an animated hamburger
        hamburgerArrowNavigator = HamburgerArrowNavigation.builder().animationDuration(250).delayDuration(20).build().getHamburgerArrowNavigator(this);
        hamburgerArrowNavigator.setupWithToolbar(mToolbar); //attach to the toolbar


    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_preferences) {
            setFragmentPreference(); //start preferences
        } else if (id == R.id.nav_about) {
            Intent i = new Intent(this, AboutActivity.class); //start about activity
            startActivity(i);
        }
        //close the drawer after selecting an option
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * show the preferences to the user
     */
    private void setFragmentPreference() {
        //Set Fragments
        setToolbarTitle(getString(R.string.preferences));
        setToolbarExpanded(false, true); //expand the toolbar
        hamburgerArrowNavigator.animateToArrow(); //animate to arrow icon on toolbar
        removeTopFragment(); //remove the top fragment if exists
        // set the preference fragment
        if (findViewById(R.id.fragment_container_list) != null) {
            //remove loadingFragment
            // Create a new Fragment to be placed in the activity layout
            MainPreferenceFragment frag = new MainPreferenceFragment();
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                frag.setEnterTransition(new Slide(Gravity.END)); // set enter transition
                Fragment prevFrag = getSupportFragmentManager().findFragmentById(R.id.fragment_container_list);
                if (prevFrag != null)
                    prevFrag.setExitTransition(new Fade(Fade.OUT)); //set exit transition
            }
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container_list, frag, SECOND_SCREEN_FRAGMENT_TAG).commit();
        }
    }

    /**
     * remove the top fragment if exists
     */
    private void removeTopFragment() {
        if (findViewById(R.id.fragment_container_score) != null) {
            Fragment fragmentById = getSupportFragmentManager().findFragmentById(R.id.fragment_container_score);
            if (fragmentById != null) {
                getSupportFragmentManager().beginTransaction()
                        .remove(fragmentById).commit();
            }
        }
    }

    /**
     * callback for onClick on an app item of the list
     * @param item the application clicked
     */
    @Override
    public void onListFragmentInteraction(Application item) {
        setToolbarTitle(item.getAppName());
        setToolbarExpanded(true, true);
        setFragmentSlaveScreen(item);
    }

    /**
     * This method obtain the user account email

     * @return the user account email if exists, null otherwise
     */
    private String getUsernameOwner() {
        AccountManager manager = AccountManager.get(this);
        //check GET_ACCOUNTS permission
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.GET_ACCOUNTS}, MY_PERMISSIONS_GET_ACCOUNTS);
            }
        }
        //collect all google accounts
        Account[] accounts = manager.getAccountsByType("com.google");
        List<String> possibleEmails = new ArrayList<>();
        for (Account a : accounts) {
            possibleEmails.add(a.name);
        }
        //if we found accounts we return the first one
        if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
            return possibleEmails.get(0);
        }
        return null;
    }

    /**
     * This method set the navigation_header. it use the user account if the permisison is granted,
     * otherwise it use a default string
     *
     * @param navigationView the navigation view used
     */
    private void setNavigationHeader(NavigationView navigationView) {
        View headerView = navigationView.getHeaderView(0);
        TextView textViewUserEMail = (TextView) headerView.findViewById(R.id.textView_user_email);
        MaterialLetterIcon letterIcon = (MaterialLetterIcon) headerView.findViewById(R.id.imageView);

        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.GET_ACCOUNTS);
        //check if we have the permission GET_ACCOUNT
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            //if we have the permisison and we find the user account
            String userEmail = getUsernameOwner();
            if (userEmail != null) {
                textViewUserEMail.setText(userEmail);
                letterIcon.setLetter(userEmail.charAt(0) + "");
            } else {
                //if we have the permission but there aren't accounts on the device
                textViewUserEMail.setText(R.string.default_user_email);
                letterIcon.setLetter("P");
            }
        } else {
            //if we haven't the permission
            textViewUserEMail.setText(R.string.default_user_email);
            letterIcon.setLetter("P");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initializeDeviceTask = new InitializeDeviceTask();
                    initializeDeviceTask.execute();
                } else {
                    initializeDeviceTask = new InitializeDeviceTask();
                    initializeDeviceTask.execute();
                }
                break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (initializeDeviceTask != null)
            initializeDeviceTask.cancel(true); //cancel the task
    }

    @Override
    protected void onResume() {
        super.onResume();
        Fragment loadFr = getSupportFragmentManager().findFragmentById(R.id.fragment_container_list);
        if (loadFr == null) {  //if fragment is showed is null
            //then show the first fragment
            setFragmentMasterScreen(null);
            setupDefaultToolbar();
        } else if (!(loadFr instanceof FragmentLoading)) {
            //if the fragment showed is not the loading fragment, then
            setupDefaultToolbar();
            if (!(loadFr instanceof FragmentList))
                //if the fragment showed is not the first screen, set the hamburger as an arrow
                hamburgerArrowNavigator.setArrow();
        }

    }

    /**
     * Set the deatil screen on the bottom fraqgment, showing the list of permissions requested by an app
     * @param selectedApplication the applicationon which you want to show informatins
     */
    public void setFragmentSlaveScreen(Application selectedApplication) {
        //Set Fragments
        // setToolbarBackIcon(false);
        hamburgerArrowNavigator.animateToArrow();
        removeTopFragment();
        if (findViewById(R.id.fragment_container_list) != null) {
            // Create a new FragmentPermission to be placed in the activity layout
            FragmentPermission frag = FragmentPermission.newInstance(selectedApplication);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container_list, frag, SECOND_SCREEN_FRAGMENT_TAG).commit();
        }
    }

    private class InitializeDeviceTask extends AsyncTask<Bundle, Void, Bundle> {
        @Override
        protected Bundle doInBackground(Bundle... bundles) {
            Device.getInstance(); //apps analysis
            if (bundles != null && bundles.length > 0)
                return bundles[0];
            return null;
        }

        @Override
        protected void onPostExecute(Bundle bundle) {
            super.onPostExecute(bundle);
            setupDefaultToolbar();
            setToolbarExpanded(false, false);
            setToolbarElevation(8);
            setFragmentMasterScreen(bundle); //show first fragment
        }
    }
}
