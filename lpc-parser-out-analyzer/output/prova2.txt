public   | org.apache.activemq.camel.CamelConnection | classe
public  | CamelContext | camelContext | variabile
protected  | CamelConnection ( Transport transport , IdGenerator clientIdGenerator , JMSStatsImpl factoryStats ) | metodo-costruttore
public  | void | setCamelContext ( CamelContext camelContext ) | metodo
public  | CamelContext | getCamelContext ( ) | metodo

public   | org.apache.activemq.camel.CamelConnectionFactory | classe
public  | CamelContext | camelContext | variabile
public  | CamelConnectionFactory ( ) | metodo-costruttore
public  | CamelContext | getCamelContext ( ) | metodo
private  | CamelConnection | createActiveMQConnection ( ) | metodo
private  | CamelConnection | createActiveMQConnection ( Transport transport , JMSStatsImpl stats ) | metodo
public  | QueueReceiver | createReceiversss ( ActiveMQSession session , String messageSelector ) | metodo

public   | org.apache.kahadb.page.PageFileTest | classe
public  | void | testCRUD ( ) | metodo
public  | void | testStreams ( ) | metodo
public  | void | testAddRollback ( ) | metodo
private | CamelContext | camelContext | variabile

