from classe import Classe
from openpyxl import load_workbook
import operator

from getClassiMod import count_class_mod


class Parser:
    def __init__(self):
        self.classi = list()

    def parseFile(self, percorso):
        with open(percorso) as f:
            lines = f.readlines()
        lines = [x.strip() for x in lines]
        for l in lines:
            # separ[-1]: ultimo elemento in lista. identifica la riga (classe, metodo, variabile)
            separ = l.split("|")
            tipo = separ[-1].strip()
            if (tipo == "classe" or tipo =="inner-class"):
                self.classi.append(Classe(separ[1], separ[0]))
            # Separ[0]: Scope + altro
            # Separ[1]: Tipo di ritorno oppure Tipo della variabile
            # Separ[2]: Nome variabile/metodo
            elif (tipo == "variabile"):
                self.classi[-1].addVariabile(separ[0], separ[1], separ[2])
            elif (tipo == "metodo"):
                self.classi[-1].addMetodo(separ[0], separ[1], separ[2])
            elif (tipo == "metodo-costruttore"):
                self.classi[-1].addMetodo(separ[0], None, separ[1])
                # for c in self.classi:
                #    print(c)
                #    c.printPrivatePubblic()



    def getClassi(self):
        return self.classi

    # metodo che confronta due liste per ottenere gli elementi che differiscono e che sono uguali:
    # return due liste la prima contiene le classi uguali, la seconda quelle diverse
    # confronto (V1, V2) ottengo le classi eliminate
    # confronto (V2, V1) ottengo le classi aggiunte
    def confronta(self, classiV1, classiV2):
        classiDiverse = []
        classiUguali = []
        for classe1 in classiV1:
            trovata = False
            for classe2 in classiV2:
                if (classe2.getNome() == classe1.getNome()):
                    trovata = True
                    break
            if (trovata):
                classiUguali.append(classe1)
            else:
                classiDiverse.append(classe1)
        return [classiUguali, classiDiverse]

    def getClassiModificate(self, classiV1, classiV2):
        # faccio il sort giusto per scrupolo, per avere la certezza che le classi uguali stanno allo stesso punto della lista
        classiV1.sort(key=operator.attrgetter('nome'))
        classiV2.sort(key=operator.attrgetter('nome'))
        classiNonModificate = []
        classiModificate = []
        # 1 confronto se le classi sono state modificate
        for c1, c2 in zip(classiV1, classiV2):
            if (c1 == c2):
                classiNonModificate.append(c1)
            else:
                classiModificate.append([c1, c2])  # mi devo portare entrambe le classi appresso per fare i confronti successivi
        # print("Classi uguali: \n")
        # for p in classiNonModificate: print(p.getNome())
        print("*****Classi modificate: "+str(len(classiModificate)))
#sers\\Assunta\\Desktop\\progettiParsare\\ACTIVEMQ\\activemq-activemq-5.8.0","C:\\Users\\Assunta\\Desktop\\progettiParsare\\ACTIVEMQ\\activemq-activemq-5.9.0")))
        # for c in classiModificate:
        #     print(c[0].getNome())
        return [classiModificate, classiNonModificate]

    def classiNonUguali(self, classiModificate):
        # 2 per ogni classe modificata vedo se sono stati modificati variabili e metodi pubblici o privati
        countMetodiPubblciAPrivati=0
        countMetodiPrivatiAPubblci=0
        countVariabiliPrivateAPubbliche=0
        countVariabiliPubblicheAPrivate=0
        countMetodiPrivatiAggiunti=0
        countMetodiPubbliciAggiunti=0
        countVariabiliPubblicheAggiunte=0
        countVariabiliPrivateAggiunte=0
        countMetodiPrivatiRimossi = 0
        countMetodiPubbliciRimossi = 0
        countVariabiliPubblicheRimosse = 0
        countVariabiliPrivateRimosse = 0
        countVariabiliUguali=0
        countVariabiliAltreModifiche=0
        countMetodiUguali=0
        countMetodiAltreModifiche=0

        for c in classiModificate:

            for variabile in list(c[0].getVariabili()):
                found=self.search(variabile, c[1].getVariabili())
                if found is not None:
                    if variabile.scope == found.scope:
                        countVariabiliUguali+=1
                    elif ("public" in variabile.scope or "package-private" in variabile.scope) and (("private" in found.scope or "protected" in found.scope) and "package-private" not in found.scope):
                        countVariabiliPubblicheAPrivate+=1
                    elif (("private" in variabile.scope or "protected" in variabile.scope)and "package-private" not in variabile.scope)  and ("public" in found.scope or "package-private" in found.scope):
                        countVariabiliPrivateAPubbliche+=1
                    else:
                        countVariabiliAltreModifiche+=1
                    c[1].getVariabili().remove(found)
                    c[0].getVariabili().remove(variabile)


            for metodo in list(c[0].getMetodi()):
                found = self.search(metodo, c[1].getMetodi())
                if found is not None:
                    if metodo.scope == found.scope:
                        countMetodiUguali += 1
                    elif ("public" in metodo.scope or "package-private" in metodo.scope) and (("private" in found.scope or "protected" in found.scope)and "package-private" not in found.scope):
                        countMetodiPubblciAPrivati += 1
                    elif (("private" in metodo.scope or "protected" in metodo.scope) and "package-private" not in metodo.scope) and ("public" in found.scope or "package-private" in found.scope):
                        countMetodiPrivatiAPubblci += 1
                    else:
                        countMetodiAltreModifiche += 1
                    c[1].getMetodi().remove(found)
                    c[0].getMetodi().remove(metodo)


            countMetodiPrivatiAggiunti+=len(c[1].getMetodiPrivati())
            countMetodiPubbliciAggiunti += len(c[1].getMetodiPubblici())
            countVariabiliPrivateAggiunte += len(c[1].getVariabiliPrivate())
            countVariabiliPubblicheAggiunte += len(c[1].getVariabiliPubbliche())
            countMetodiPrivatiRimossi += len(c[0].getMetodiPrivati())
            countMetodiPubbliciRimossi += len(c[0].getMetodiPubblici())
            countVariabiliPrivateRimosse += len(c[0].getVariabiliPrivate())
            countVariabiliPubblicheRimosse += len(c[0].getVariabiliPubbliche())

        print("\nMetodi da pubblci a privati: "+str(countMetodiPubblciAPrivati))
        print("Metodi da privati a pubblici: " + str(countMetodiPrivatiAPubblci))
        print("Metodi con altre modifiche: "+ str(countMetodiAltreModifiche))

        print("\nVariabili da private a pubbliche: "+ str(countVariabiliPrivateAPubbliche))
        print("Variabili da pubbliche a private: "+ str(countVariabiliPubblicheAPrivate))
        print("Variabili con altre modifiche: "+ str(countVariabiliAltreModifiche))


        print("\nMetodi pubblici aggiunti da classi modificate: " + str(countMetodiPubbliciAggiunti))
        print("Metodi privati aggiunti da classi modificate: " + str(countMetodiPrivatiAggiunti))
        print("Variabili private aggiunte da classi modifichate: " + str(countVariabiliPrivateAggiunte))
        print("Vaiabili pubbliche aggiunte da classi modifichate: " + str(countVariabiliPubblicheAggiunte))
        print("\nMetodi pubblci rimossi da classi modificate: " + str(countMetodiPubbliciRimossi))
        print("Metodi privati rimossi da classi modificate: " + str(countMetodiPrivatiRimossi))
        print("Variabili private rimosse da classi modifichate: " + str(countVariabiliPrivateRimosse))
        print("Vaiabili pubbliche rimosse da classi modifichate: " + str(countVariabiliPubblicheRimosse))
        # print("\nMetodi invariati: " + str(countMetodiUguali))
        # print("Variabili invariate: " + str(countVariabiliUguali))

        return [countMetodiPrivatiAPubblci, countMetodiPubblciAPrivati, countVariabiliPrivateAPubbliche, countVariabiliPubblicheAPrivate,
                countMetodiPrivatiAggiunti,countMetodiPubbliciAggiunti, countVariabiliPrivateAggiunte,countVariabiliPubblicheAggiunte,
                countMetodiPrivatiRimossi,countMetodiPubbliciRimossi,countVariabiliPrivateRimosse,countVariabiliPubblicheRimosse]

    def search(self,elem, list):
        for x in list:
            if elem.__eq__(x):
                return x
        return None
    def classiUguali(self,ClassiNonModificate):
        for coppie in ClassiNonModificate:
            primaClasse=coppie[0]
            secondaClasse=coppie[1]



    def countTotali(self,classiV1):
        countTotaliMetodiPrivati=0
        countTotaliMetodiPubblici=0
        countTotaliVariabiliPrivate=0
        countTotaliVariabiliPubbliche=0
        totaleMetodi=0
        totaleVariabili=0
        for c in classiV1:
            countTotaliMetodiPubblici+=len(c.getMetodiPubblici())
            countTotaliMetodiPrivati+=len(c.getMetodiPrivati())
            countTotaliVariabiliPubbliche+=len(c.getVariabiliPubbliche())
            countTotaliVariabiliPrivate+=len(c.getVariabiliPrivate())
            totaleMetodi += len(c.getMetodi())
            totaleVariabili += len(c.getVariabili())


        # print("\nTotali metodi Pubblici "+str(countTotaliMetodiPubblici))
        # print("Totali metodi Privati "+str(countTotaliMetodiPrivati))
        # print("Totali variabili pubbliche "+str(countTotaliVariabiliPubbliche))
        # print("Totali variabili private "+str(countTotaliVariabiliPrivate))
        # print("Totale metodi: "+str(totaleMetodi))
        # print("Totale variabili "+str(totaleVariabili))

        return [totaleMetodi, totaleVariabili, countTotaliMetodiPrivati,countTotaliMetodiPubblici,countTotaliVariabiliPrivate,countTotaliVariabiliPubbliche]



# TODO mettere nel main
primoFile = 'output/ActiveMQ/activemq-activemq-5.8.0.txt'
secondoFile = 'output/ActiveMQ/activemq-activemq-5.9.0.txt'
nome1="5.8.0"
nome2="5.9.0"

# primoFile = 'output\\prova1.txt'
# secondoFile = 'output\\prova2.txt'

parser = Parser()
parser.parseFile(primoFile)
classiV1 = parser.getClassi()
numeroClassiV1= len(classiV1)

parser2 = Parser()
parser2.parseFile(secondoFile)
classiV2 = parser2.getClassi()
numeroClassiV2=len(classiV2)


ret = parser.confronta(classiV1, classiV2)
classiEliminate = ret[1]

print("****classi eliminate: "+str(len(classiEliminate)))
# for p in classiEliminate:
#     print(p.getNome())
classiComuniV1 = ret[0]
ret = parser2.confronta(classiV2, classiV1)
classiComuniV2 = ret[0]
classiAggiunte = ret[1]
print("\n****classiAggiunte: "+str(len(classiAggiunte)))
# for p in classiAggiunte:
#     print(p.getNome())


totali=parser.countTotali(classiV1)
totalieliminate= parser.countTotali(classiEliminate)
totaliaggiunte= parser.countTotali(classiAggiunte)
risultato_analisi_classi_in_entrambe_versioni= parser.getClassiModificate(classiComuniV1, classiComuniV2)
classiModificate=risultato_analisi_classi_in_entrambe_versioni[0]
classiNonModificate=risultato_analisi_classi_in_entrambe_versioni[1]
#countTotaleMetodiModificati(classiModificate)
#N.B fare parser.classiUguali alla fine perche' rimuove le cose dalle liste e quindi poi non ti trovi piu' con i conti
resultPP=parser.classiNonUguali( classiModificate)


#stampa
wb=load_workbook("result.xlsx")
ws=wb.active
ws.append([nome1+"-"+nome2,len(classiEliminate), len(classiAggiunte),len(risultato_analisi_classi_in_entrambe_versioni[0]),numeroClassiV1,numeroClassiV2, totali[0], totali[1], totali[2], totali[3], totali[4], totali[5],
           totalieliminate[0], totalieliminate[1], totalieliminate[2], totalieliminate[3], totalieliminate[4], totalieliminate[5],
           totaliaggiunte[0], totaliaggiunte[1], totaliaggiunte[2], totaliaggiunte[3], totaliaggiunte[4], totaliaggiunte[5],
            resultPP[0], resultPP[1], resultPP[2], resultPP[3],
            resultPP[4], resultPP[5], resultPP[6], resultPP[7],
            resultPP[8], resultPP[9], resultPP[10], resultPP[11]])
wb.save("result.xlsx")









