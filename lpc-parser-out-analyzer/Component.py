class Component:
    def __init__(self, scope, tipo, name):
        self.tipo = tipo

        self.nome = name
        self.scope = scope

    def __str__(self):
        if self.tipo is None:
            return 'Scope:' + self.scope + 'Nome: ' + self.nome

        return 'Scope:' + self.scope + 'Tipo: ' + self.tipo + 'Nome: ' + self.nome

    def __eq__(self, other):
        return self.nome == other.nome and self.tipo == other.tipo

    def __hash__(self):
        return hash((self.tipo, self.nome,self.scope))
