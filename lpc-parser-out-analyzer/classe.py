from Component import Component


class Classe:
    def __init__(self, name, scope):
        self.nome = name
        self.scope = scope
        self.metodiPrivati=list()
        self.metodiPubblici=list()
        self.variabiliPubbliche=list()
        self.variabiliPrivate=list()
        self.metodi = list()
        self.variabili = list()

    def addMetodo(self,scope, tipo, name):
        metodo = Component(scope, tipo, name)
        self.metodi.append(metodo)
        if "private" in scope:
            self.metodiPrivati.append(metodo)
        elif "public" in scope:
            self.metodiPubblici.append(metodo)

    def addVariabile(self, scope, tipo, name):
        variabile = Component(scope, tipo, name)
        self.variabili.append(variabile)
        if "private" in scope:
            self.variabiliPrivate.append(variabile)
        elif "public" in scope:
            self.variabiliPubbliche.append(variabile)

    def __str__(self):
        return 'Scope:' + self.scope + 'Nome Classe: ' + self.nome +'\nVariabili\n\t'+ '\n\t'.join(map(str, self.variabili))+'\nMetodi\n\t'+ '\n\t'.join(map(str, self.metodi))

    def printPrivatePubblic(self):
        print("Metodi privati: "+ str(len(self.metodiPrivati)))
        print("Metodi pubblici: " + str(len(self.metodiPubblici)))
        print("Variabili private: " + str(len(self.variabiliPrivate)))
        print("Variabili pubbliche: " + str(len(self.variabiliPubbliche)))

    def printAllMetodiVariabili(self):
        print("Variabili private:")
        for v in self.variabiliPrivate:
            print v
        print("Variabili pubbliche:")
        for v in self.variabiliPubbliche:
            print v
        print ("Metodi privati: ")
        for m in self.metodiPrivati:
            print m
        print ("Metodi pubblici: ")
        for m in self.metodiPubblici:
            print m
    def getNome(self):
        return self.nome

    def getMetodiPrivati(self):
        ret=list()
        for metodo in self.metodi:
            if ("private" in metodo.scope or "protected" in metodo.scope) and "package-private" not in metodo.scope:
                ret.append(metodo)
        return ret
    def getMetodiPubblici(self):
        ret = list()
        for metodo in self.metodi:
            if "public" in metodo.scope or "package-private" in metodo.scope:
                ret.append(metodo)
        return ret
    def getVariabiliPrivate(self):
        ret = list()
        for variabile in self.variabili:
            if ("private" in variabile.scope or "protected" in variabile.scope) and "package-private" not in variabile.scope:
                ret.append(variabile)
        return ret
    def getVariabiliPubbliche(self):
        ret = list()
        for variabile in self.variabili:
            if "public" in variabile.scope or "package-private" in variabile.scope:
                ret.append(variabile)
        return ret
    def getMetodi(self):
        return self.metodi
    def getVariabili(self):
        return self.variabili

    def __eq__(self, other):
        return (self.nome == other.nome) and (self.scope == other.scope) and (
            set(self.variabili) == set(other.variabili)) and (set(self.metodi) == set(other.metodi))

